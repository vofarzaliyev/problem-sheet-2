# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    if all(letter in letters_guessed for letter in secret_word):
        return True
    else:
        return False


def get_guessed_word(secret_word, letters_guessed):
    secret_word_as_list = list(secret_word)
    result = ""
    for i in range(len(secret_word)):
        if secret_word_as_list[i] in letters_guessed:
            result += secret_word_as_list[i]
        else:
            result += "_ "
    return result



def get_available_letters(letters_guessed):
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    alphabet_list = list(alphabet)
    for letter in letters_guessed:
        alphabet_list.remove(letter)
    return ''.join(alphabet_list)

    

def hangman(secret_word):
    print("Welcome to the game Hangman!")
    print("I am thinking of word that is {0} letters long.".format(len(secret_word)))
    number_of_warnings = 3
    print("You have {0} warnings left".format(number_of_warnings))
    print("-------------")
    number_of_guesses = 6

    letters_guessed = []
    while (number_of_guesses != 0):
        print("You have {0} guesses left".format(number_of_guesses))
        
        try:
            print("Available letters: "+get_available_letters(letters_guessed))            
        except ValueError:
            number_of_warnings -=1
            if number_of_warnings >=0:
                letters_guessed.pop()
                print("Oops! That is not a valid letter. You have {0} warnings left: ".format(number_of_warnings)+get_guessed_word(secret_word, letters_guessed))
            else:
                print("You have no warning, you lose a guess.")
                number_of_guesses -=1
        
        guessed_letter = (input("Please guess a letter: ")).lower()        
        letters_guessed.append(guessed_letter)
        
        if letters_guessed[-1] in list(secret_word):
            print("Good guess: "+ get_guessed_word(secret_word, letters_guessed))
        else:
            print("Oops! That letter is not in my word: "+get_guessed_word(secret_word, letters_guessed))
        
        number_of_guesses -= 1
        total_score = number_of_guesses * len(set(list(secret_word)))
        if is_word_guessed(secret_word, letters_guessed):
            print( "Congratulations, you won!")
            print( "Your total score for this game is: "+ total_score)
            return True
        print("-------------")        

    
    print("Sorry, you ran out of guesses. The word was " + secret_word +".")
    
    return False






# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):
    unknown_letters = []
    my_word = my_word.replace(" ", "")
    if len(my_word) == len(other_word):
        for i in range(len(my_word)):
            if list(my_word)[i] != "_":
                if list(my_word)[i] != list(other_word)[i]:
                    return False
            else:
                unknown_letters.append(other_word[i])        
        if any(x in list(my_word) for x in unknown_letters):
            return False
        else:
            return True
    else:
        return False

print("hello")
print(match_with_gaps("a_ ple","apple"))

def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    my_word = my_word.replace(" ","")
    filtered_words = []
    for word in wordlist:
        if len(word) == len(my_word) and all(word[i] == my_word[i] or my_word[i] == "_" for i in range(len(list(my_word)))):
            filtered_words.append(word)

    if len(filtered_words) == 0:
        return "no matches found"
    else:
        return filtered_words

        



def hangman_with_hints(secret_word):
    print("Welcome to the game Hangman!")
    print("I am thinking of word that is {0} letters long.".format(len(secret_word)))
    number_of_warnings = 3
    print("You have {0} warnings left".format(number_of_warnings))
    print("-------------")
    number_of_guesses = 6

    letters_guessed = []
    while (number_of_guesses != 0):
        print("You have {0} guesses left".format(number_of_guesses))
        
        try:
            print("Available letters: "+get_available_letters(letters_guessed))            
        except ValueError:
            number_of_warnings -=1
            if number_of_warnings >=0:
                letters_guessed.pop()
                print("Oops! That is not a valid letter. You have {0} warnings left: ".format(number_of_warnings)+get_guessed_word(secret_word, letters_guessed))
            else:
                print("You have no warning, you lose a guess.")
                number_of_guesses -=1
        
        guessed_letter = (input("Please guess a letter: ")).lower()
        if guessed_letter == "*":
            print("Possible word matches are:")
            print(show_possible_matches(get_guessed_word(secret_word, letters_guessed)))
            number_of_guesses +=1
        else:
            letters_guessed.append(guessed_letter)
        
        if letters_guessed != [] and letters_guessed[-1] in list(secret_word):
            print("Good guess: "+ get_guessed_word(secret_word, letters_guessed))
        elif letters_guessed == []:
            print("You haven't guessed a letter yet")         
        else:
            print("Oops! That letter is not in my word: "+get_guessed_word(secret_word, letters_guessed))
        
        number_of_guesses -= 1
        total_score = number_of_guesses * len(set(list(secret_word)))
        if is_word_guessed(secret_word, letters_guessed):
            print( "Congratulations, you won!")
            print( "Your total score for this game is: "+ total_score)
            return True
        print("-------------")        

    
    print("Sorry, you ran out of guesses. The word was " + secret_word +".")
    
    return False


if __name__ == "__main__":

    
    #secret_word = choose_word(wordlist)
    #hangman(secret_word)


    
    secret_word = choose_word(wordlist)
    hangman_with_hints(secret_word)